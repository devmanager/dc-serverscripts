group_prefix=dc-

# Loop through all groups that begin with the prefix
for group in $(grep "$group_prefix" /etc/group); do
	gname=$(expr $group : "\($group_prefix[^:]\+\)");
done
