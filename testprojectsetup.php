<?php
// Include tools to help in building the response
require_once('makeResponse.php');
$shell = isset($argv[1]) ? escapeshellarg($argv[1]) : '';

/*
The PATH isn't changed by ~/bash.profile when the web app runs this
*/
exec("~/devmanager/testprojectsetup.sh $shell 2>&1", $output, $code);
$line = $output[0];
if($code){
	$line = "Error: $line";
}

makeResponse($line);

