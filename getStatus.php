<?php
// Include tools to help in building the response
require_once('makeResponse.php');
$rawUser = $argv[1];
$user = isset($argv[1]) ? escapeshellarg($argv[1]) : '';
$data = [];

/*
 * Make sure user belongs to a project group
 */
$group_prefix = 'dc-';
exec("grep $group_prefix /etc/group 2>&1", $output, $code);

if($code !== 0){
	echo "Error: " . $output[0];
	exit();
}

$userfound = false;
$group = '';
foreach($output as $line){	
	$names = explode(':', $line);
	$groupKey = count($names) - 1;
	
	if(!$names[$groupKey]){
		continue;
	}
	$groupList = explode(',', $names[$groupKey]);
	$groupNameKey = array_search($rawUser, $groupList);
	if($groupNameKey !== false){
		$group = $names[0]; 
		$userfound = true;
		break;
	}
}

if(!$userfound){
	echo "Error: Could not find user $user in any project groups";
	exit();
}

exec("getent passwd $user | cut -d : -f 7 2>&1", $output, $code);
if($code !== 0){
	echo "Error: " . $output[0];
	exit();
}
$shell = $output[0];

$groupName = preg_replace("/^$group_prefix/", '', $group);

$projectdir = "/var/www/$groupName";

$devconf = @file_get_contents('/etc/httpd/conf.d/devcamp/devcamp.conf');

$matches = [];
$found = preg_match("/ServerName\s+(.*$groupName\S+)/", $devconf, $matches);

if(isset($matches[1])){
	$projecturl = 'http://' . $matches[1];
} else {
	echo "Error: Could not find project httpd configuration";
	exit();
}

$data = [ 
		'User' => $rawUser,
		'Group' => $group,
		'Project Directory' => $projectdir,
		'Project URL' => $projecturl
];

$message = "Success: Project Status retrieved";
makeResponse($message, $data);
