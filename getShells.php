<?php
// Include tools to help in building the response
require_once('makeResponse.php');

/*
 Get the file containing the expected shells
 */
$jsonfile = '/home/setupadmin/devmanager/shells.json';
$data = file_get_contents($jsonfile);
if(! $shelllist = json_decode($data, true)){
	echo "Error: Could not decode JSON from $jsonfile";
	exit();
}

/*
 * Only return the shell programs that are confirmed to exist
 */
$confirmedlist = array();
foreach($shelllist as $shell){
	exec("[[ -e $shell ]] 2>&1", $output, $code);
	
	if($code !== 0){
		echo "Error: " . $output[0];
		exit();
	}
	
	$confirmedlist[] = $shell;
}

$message = "Success: Shell list retrieved";
$data = json_encode($confirmedlist);

makeResponse($message, $data);