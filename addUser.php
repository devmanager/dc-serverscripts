<?php
// Include tools to help in building the response
require_once('makeResponse.php');
$user = isset($argv[1]) ? escapeshellarg($argv[1]) : '';
$pass = isset($argv[2]) ? escapeshellarg($argv[2]) : '';
$group = isset($argv[3]) ? escapeshellarg($argv[3]) : '';
$mgroup = isset($argv[4]) ? escapeshellarg($argv[4]) : '';
$shell = isset($argv[5]) ? escapeshellarg($argv[5]) : '';

exec("~/devmanager/createUser.sh $user $pass $group $mgroup $shell 2>&1", $output, $code);
$line = $output[0];
if($code){
	$line = "Error: $line";
}

makeResponse($line);