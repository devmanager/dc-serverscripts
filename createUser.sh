#!/bin/bash

user=$1
pass=$2
group=$3
mgroup=$4
shell=$5

if ! ~/devmanager/addUser.sh "$user" "$pass" "$group" "$mgroup" "$shell"; then
	exit 1;
fi

echo "Success: User ""$1"" created"