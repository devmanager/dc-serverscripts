<?php
/**
   Format the data output from web server scripts in a way that the Dev Manager 
   application will understand.

   @param string $message The message returned from the script, prepended with the status
                          followed by a colon.
   @param mixed $data Any data that is expected with the response.
*/
function makeResponse($message = '', $data = null){
	preg_match('/^(.+?):(.*)/i', $message, $matches);
	$status = $matches[1];
	$mesg = trim($matches[2]);

	$response = array('status' => $status, 'message' => $mesg);
	
	if($data){
		$response['data'] = $data;
	}

	echo json_encode($response);
}

