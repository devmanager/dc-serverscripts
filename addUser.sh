#!/bin/bash

user=$1
pass=$2
group=$3
mgroup=$4
shell=$5

# Create the user in the specified group with the given shell
e=$(sudo useradd "$user" -G "$group","$mgroup" -s "$shell" 2>&1)
if [[ -n "$e" ]]; then	
	echo "Error: $e"
	exit 1
fi

# Create the user's password
e=$(echo "$pass" | sudo passwd --stdin "$user" 2>&1)
if ! [[ "$e" =~ ^Changing ]]; then	
	echo "Error: $e"
	exit 1
fi