<?php
// Include tools to help in building the response
require_once('makeResponse.php');
$group = isset($argv[1]) ? escapeshellarg($argv[1]) : '';

/*
The PATH isn't changed by ~/bash.profile when the web app runs this
*/
exec("~/devmanager/testsetup.sh $group 2>&1", $output, $code);
$line = $output[0];
if($code){
	$line = "Error: $line";
}

makeResponse($line);

