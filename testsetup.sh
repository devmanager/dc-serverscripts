group="$1"

# Create a test user in the specified group
e=$(sudo useradd -G "$group" testuser 2>&1)
if [[ -n "$e" ]]; then	
	echo "Error: $e"
	exit 1
fi


# Delete the user
e=$(sudo userdel -r testuser 2>&1)
if [[ -n "$e" ]]; then	
	echo "Error: $e"
	exit 2
fi

echo "Success: Test user created and removed"
