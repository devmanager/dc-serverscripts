# Create a test user in the specified group

group_prefix=dc-
default_shell=/bin/$1

# Loop through all groups that begin with the prefix
for group in $(grep "$group_prefix" /etc/group); do
	# Create and then remove the test user
	gname=$(expr $group : "\($group_prefix[^:]\+\)");
	e=$(sudo useradd -G "$gname" testuser -s "$default_shell" 2>&1)
	if [[ -n "$e" ]]; then	
		echo "Error: $e"
		exit 1
	fi
	
	# Check the existence of the project directory
	projdir=/var/www/${gname/$group_prefix}
	if [[ ! -e "$projdir" ]]; then
		echo "Error: Project directory $projdir does not exist"
		exit 2 
	fi

	# Delete the user
	e=$(sudo userdel -r testuser 2>&1)
	if [[ -n "$e" ]]; then	
		echo "Error: $e"
		exit 3
	fi
done

echo "Success: Created and removed test users"
