<?php
// Include tools to help in building the response
require_once('makeResponse.php');

/*
The PATH isn't changed by ~/bash.profile when the web app runs this
*/
$group_prefix = 'dc-';
exec("grep $group_prefix /etc/group 2>&1", $output, $code);

if($code !== 0){
    echo "Error: " . $output[0];
    exit();
}

/*
Get the file containing the expected group names and their labels
*/
$jsonfile = '/home/setupadmin/devmanager/groups.json';
$data = file_get_contents($jsonfile);
if(! $grouplist = json_decode($data, true)){
	echo "Error: Could not decode JSON from $jsonfile";
	exit();
}

/*
Only return groups from the list that are found on the system
*/
$confirmedlist = array();
foreach($output as $line){
	$groupList = explode(':', $line);
	$group = $groupList[0];
	if(!empty($grouplist[$group])){
		$confirmedlist[$group] = $grouplist[$group];
	}
}

$message = "Success: Group list retrieved";
$data = json_encode($confirmedlist);

makeResponse($message, $data);

